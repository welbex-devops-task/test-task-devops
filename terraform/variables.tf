variable "aws_region" {
  type        = string
  default     = "eu-central-1"
  description = "AWS region"
}

variable "vpc_cidr" {
    type        = string
    default     = "10.0.0.0/16"
    description = "cidr for vpc"
}


variable "azs" {
    type = list
    default = ["eu-central-1a", "eu-central-1b"]
}

variable "cert_arn" {
    type = string
    default = "arn:aws:acm:eu-central-1:406496705539:certificate/c2d7362d-695e-4341-b80b-6a010b149b11"
}


variable "pub_subs" {
    type    = list
    default = ["10.0.11.0/24", "10.0.12.0/24"]
}

variable "private_subs" {
    type    = list
    default = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "key_name" {
  type    = string
  default = "welbex-key"
}

variable "prefix" {
  type        = string
  default     = "welbex-"
  description = "prefix for names"
}

