module "alb" {
  source  = "terraform-aws-modules/alb/aws"

  name = "${var.prefix}alb"

  load_balancer_type = "application"

  vpc_id             = module.vpc.vpc_id
  subnets            = ["${module.vpc.public_subnets[0]}", "${module.vpc.public_subnets[1]}"]
  security_groups    = ["${module.lb_https_443_security_group.security_group_id}", "${module.lb_http_80_security_group.security_group_id}"]



  target_groups = [
    {
      name_prefix      = "welb"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
      targets = [
        {
          target_id = "${module.ec2_instance.id}"
          port = 80
        }
      ]

    }
  ]

  https_listeners = [
    {
      port                 = 443
      protocol             = "HTTPS"
      certificate_arn      = var.cert_arn
    }
  ]

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

}