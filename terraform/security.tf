module "ec2_ssh_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/ssh"
  name   = "${var.prefix}ec2-ssh-sg"
  vpc_id = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "ec2_http_80_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/http-80"

  name   = "${var.prefix}ec2-http-sg"
  vpc_id = module.vpc.vpc_id
  ingress_cidr_blocks = ["${module.vpc.vpc_cidr_block}"]
}


module "lb_https_443_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/https-443"

  name   = "${var.prefix}lb-https-sg"
  vpc_id = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]

}

module "lb_http_80_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/http-80"

  name   = "${var.prefix}lb-http-sg"
  vpc_id = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
}