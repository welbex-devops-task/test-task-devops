module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  name = "${var.prefix}app-instance"

  ami                    = "${data.aws_ami.amazon_linux.id}"
  instance_type          = "t2.micro"
  key_name               = "${aws_key_pair.kp.key_name}"
  vpc_security_group_ids = ["${module.ec2_ssh_sg.security_group_id}", "${module.ec2_http_80_security_group.security_group_id}"]
  subnet_id              = "${module.vpc.public_subnets[0]}"
  
  
}