module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.prefix}vpc"
  cidr = var.vpc_cidr

  azs             = var.azs
  private_subnets = var.private_subs
  public_subnets  = var.pub_subs

  

  enable_dns_hostnames = true
  
}